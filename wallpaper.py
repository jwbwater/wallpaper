"""
Cycle through wallpaper pictures.
"""
import click
import os
import random as rand
import re
import subprocess
import sys
import time


@click.group()
def wallpaper():
    """ Command line desktop wallpaper manager for XFCE. """


@wallpaper.command()
@click.argument('filepath')
def change(filepath):
    """ Use the given file as the desktop wallpaper. """
    change_wallpaper(filepath)


def change_wallpaper(filepath):
    """ Change wallpaper to the image specified by filepath. """
    # change workspace backdrop
    subprocess.run(
        "xfconf-query -c xfce4-desktop -p " + workspace() + " -s " + filepath,
        shell=True)


@wallpaper.command()
@click.option('-p', '--period', default=3600)
@click.option('-d', '--directory')
def cycle(period, directory):
    """ Periodically change desktop wallpaper in XFCE """
    if os.fork():
        # If os.fork is not 0 then this is the parent process.
        # Exit parent process to leave only the background, child process.
        os._exit(os.EX_OK)
    while True:
        change_wallpaper(random_image(directory))
        time.sleep(period)


@wallpaper.group(invoke_without_command=True)
def file():
    """ Print the file path to the current wallpaper image. """
    print(_filepath())


def _filepath():
    """ Return the file path to the current wallpaper image. """
    process = subprocess.Popen(
        "xfconf-query -c xfce4-desktop -p " + workspace(),
        stdout=subprocess.PIPE,
        stderr=subprocess.PIPE,
        shell=True)
    stdin, stderr = process.communicate()
    return stdin.strip().decode()


@wallpaper.command()
@click.argument('destination')
def mv(destination):
    """ Move the file currently used as the wallpaper image. """
    subprocess.run(['mv', filepath(), destination])


@wallpaper.command()
@click.option('-d', '--directory')
def random(directory):
    """ Change the desktop wallpaper to a random image. """
    change_wallpaper(random_image(directory))


def random_image(directory=None):
    """ Choose a random wallpaper image. """
    if not directory:
        USER = os.environ['USER']
        directory = "/home/{}/Pictures/Wallpaper".format(USER)
    if not os.path.isdir(directory):
        sys.exit("Wallpaper directory, {}, does not exist!".format(directory))
    ls_directory = subprocess.Popen(
        ["ls", "-w1", directory],
        stdout=subprocess.PIPE,
        stderr=subprocess.PIPE)
    stdout, err = ls_directory.communicate()
    filepath = (
        directory + '/' +
        rand.choice(stdout.decode().split('\n')).replace(' ', '\ '))
    return filepath


def workspace():
    """ Return the XFCE workspace string for xfconf-query. """
    backdrops = subprocess.Popen(
        ["xfconf-query", "-c", "xfce4-desktop", "-p", "/backdrop", "-lv"],
        stdout=subprocess.PIPE,
        stderr=subprocess.PIPE)
    stdout, stderr = backdrops.communicate()
    path = re.search("/backdrop.*HDMI.*last-image", stdout.decode())[0]
    # patch for a change in the output of xfconf-query
    return path.replace('HDMI-', 'HDMI')
