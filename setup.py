from setuptools import setup

setup(
    name='wallpaper',
    version='0.1.0',
    description='Desktop wallpaper rotator.',
    py_modules=['wallpaper'],
    include_package_data=True,
    zip_safe=False,
    install_requires=[
        'click',
    ],
    entry_points={
        'console_scripts': [
            'wallpaper = wallpaper:wallpaper'
        ]
    },
)
