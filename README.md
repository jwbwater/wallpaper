# Command line interface for pcloud
CLI to change or rotate desktop wallpaper images on XFCE.

## Installation
```
git clone git@gitlab.com:jwbwater/wallpaper.git
sudo pip3 install .
```

## Usage
```
wallpaper change <file path>
wallpaper cycle
wallpaper file
wallpaper random
```
